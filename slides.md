---
# try also 'default' to start simple
theme: seriph
# random image from a curated Unsplash collection by Anthony
# like them? see https://unsplash.com/collections/94734566/slidev
background: https://source.unsplash.com/-XvhrIC1Mhc/1920x1080
# apply any windi css classes to the current slide
class: 'text-center'
# https://sli.dev/custom/highlighters.html
highlighter: shiki
# some information about the slides, markdown enabled
info: |
  # Ruby Nights 2021
  Presentation slides for Ruby Auckland meetup details.

  Learn more at [meetup.com](https://www.meetup.com/aucklandruby/)
---

# Welcome to Auckland Ruby

---
layout: center
---

# House rules

- restroom 🚻
- fire 🔥🚪 exit

---
layout: image-right
image: ./images/minions-1024x768.jpg
class: 'text-center flex flex-col justify-center items-center'
---

# Member of Ruby NZ?

<div class="text-center flex flex-col justify-center items-center">
  Join us now! Head over to ruby.nz to find out more.
</div>

---
layout: center
class: text-center
---

# Agenda

<dl class="text-left space-y-2">
  <div>
    <dt class="font-mono">6:30 – 6:45pm</dt>
    <dd class="text-xl">👋🏾👋🏼👋🏽 Welcome 👋👋🏿👋🏻</dd>
  </div>
  <div>
    <dt class="font-mono">6:45pm – 7:30pm</dt>
    <dd class="text-xl">Food 🍕 & socialising 💬</dd>
  </div>
  <div>
    <dt class="font-mono">7:15 – 8:00pm</dt>
    <dd class="text-xl">Rob Irwin: Fish shell</dd>
  </div>
  <div>
    <dt class="font-mono">8:15 – 9:00pm</dt>
    <dd class="text-xl">Eaden McKee: Coding like DHH in 2024; zellij and lazyvim + state of the Terminal UI in 2024 in the form of a curated selection of great programs</dd>
  </div>
  <div>
    <dt class="font-mono">9:00pm – 9:20pm</dt>
    <dd class="text-xl">More socialising 💬</dd>
  </div>
  <div>
    <dt class="font-mono">9:20pm – 9:30pm</dt>
    <dd class="text-xl">Cleanup & Goodbye</dd>
  </div>
</dl>

---
layout: center
class: text-center
---

# We have amazing sponsors!

<div class="text-xl">🙏🎉🙏🏻❤️🙏🏼✨🙏🏽❤️🙏🏾🎉🙏🏿</div>

---
layout: center
class: 'text-center flex flex-col justify-center items-center'
---

![Paloma logo](/images/paloma.svg)

<div class="text-3xl text-gray-500 text-bold">
  for sponsoring food & venue
</div>

<p class="text-3xl text-dark-100 text-bold pt-5" hidden>
  ✨ Hiring a couple of engineering roles. Full-stack Ruby/Rails experience is definitely of interest. ✨
</p>

<style>
  img {
    width: 100vh;
  }
</style>

---
layout: center
class: 'text-center flex flex-col justify-center items-center'
---

![Hashbang logo](/images/hashbang-logo-pink.svg)

<div class="text-3xl text-red-400 text-bold">for sponsoring beverages</div>

<p class="text-3xl text-dark-100 text-bold pt-5" hidden>
  ✨ Looking for contractor with Data Visualization skills ✨
</p>

<style>
  img {
    width: 100vh;
  }
</style>

---
layout: center
class: 'text-center flex flex-col justify-center items-center'
---

![](/images/docspring-logo.svg)

<div class="text-3xl text-bold">
  for sponsoring education materials and swag
</div>

<p class="text-3xl text-bold pt-5" hidden>
  DocSpring are looking for…
</p>

<style>
  img {
    width: 100vh;
  }

  p {
    color: #0D1A4D;
  }
</style>


---
layout: center
class: 'text-center flex flex-col justify-center items-center'
---

# Unfolding Group Ltd.*

*yours truly

<div class="flex justify-center">
  <img src="/images/unfolding_markets.svg" alt="Unfolding logo" />
</div>

<div class="text-3xl text-teal-300 text-bold">
  for the Meetup membership cost
</div>

<style>
  img {
    max-height: 40vh;
  }
</style>

---
layout: center
class: text-center
---

# Please consider contributing

<div class="text-3xl text-dark-100 text-bold pt-5">
  <p>Give a talk, run a code session, other type of event?</p>
</div>

<div class="flex justify-center">
  <img src="/images/doodle.svg" alt="Doodle QR code" />
</div>

<style>
  img {
    height: 30vh;
  }
</style>
